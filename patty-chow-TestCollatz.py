#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "201 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  201)
        self.assertEqual(j, 210)

    def test_read_4(self):
        s = "1000 900\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1000)
        self.assertEqual(j, 900)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_largerange_1(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_largerange_2(self):
        v = collatz_eval(2, 999989)
        self.assertEqual(v, 525)

    def test_largerange_3(self):
        v = collatz_eval(3, 999799)
        self.assertEqual(v, 525)

    def test_smallrange_1(self):
        v = collatz_eval(3, 4)
        self.assertEqual(v, 8)

    def test_smallrange_2(self):
        v = collatz_eval(500033, 500035)
        self.assertEqual(v, 152)

    def test_smallrange_3(self):
        v = collatz_eval(999998, 999999)
        self.assertEqual(v, 259)

    def test_norange_1(self):
        v = collatz_eval(3, 3)
        self.assertEqual(v, 8)

    def test_norange_2(self):
        v = collatz_eval(500033, 500033)
        self.assertEqual(v, 152)

    def test_norange_3(self):
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259)

    def test_reverse_1(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_reverse_2(self):
        v = collatz_eval(200, 100)
        self.assertEqual(v, 125)

    def test_reverse_3(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)


    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")
    
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1000, 900, 174)
        self.assertEqual(w.getvalue(), "1000 900 174\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    
    def test_solve_2(self):
        r = StringIO("1 999999\n2 999989\n3 999799\n3 4\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 999999 525\n2 999989 525\n3 999799 525\n3 4 8\n")
        
    def test_solve_3(self):
        r = StringIO("500033 500035\n999998 999999\n3 3\n1 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "500033 500035 152\n999998 999999 259\n3 3 8\n1 1 1\n")
        
    def test_solve_4(self):
        r = StringIO("3804 3804\n10 1\n200 100\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3804 3804 31\n10 1 20\n200 100 125\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
